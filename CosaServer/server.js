﻿var mosca = require('mosca');
var jwt = require('jsonwebtoken');
var forecast = require('./forecast.js');


var knownChannels = new Map([
    ['besiktas', { latitude: 41.044004, longitude: 28.9998113 }]
]);

var MQTT_QoS = {
    atMostOnce: 0,
    atLeastOnce: 1,
    exactlyOnce: 2
};

class CosaServerSettings {
    constructor(serverSettings, forecastSettings) {
        this.serverSettings = serverSettings;
        this.forecastSettings = forecastSettings;
    }
}

class CosaServer {
    /**
     * @param settings CosaServerSettings
     */
    constructor(settings = null) {
        this.settings = settings;
    }

    /**
     * Initialize CosaServer
     */
    init() {
        //Initialize weather forecast provider (in this example Dark Sky Api)
        this.forecastProvider = new forecast.CosaForecastProvider(this.settings.forecastSettings);

        //Initilalize mosca server
        this.server = new mosca.Server(this.settings.serverSettings);

        this.addServerListeners();
    }

    /**
     * Add Listeners to Initialized Server
     */
    addServerListeners() {
        this.server.on('ready', this.onServerReady.bind(this));
        this.server.on('clientConnected', this.onClientConnected.bind(this));
        this.server.on('published', this.onPublished.bind(this));
        this.server.on('subscribed', this.onSubscribed.bind(this));
        this.server.on('unsubscribed', this.onUnsubscribed.bind(this));
    }

    onServerReady() {
        console.log('CosaServer - Server Ready');
        this.server.authenticate = this.clientAuthauthentication;
    }

    onClientConnected(client) {
        console.log('CosaServer - Client Connected:', client.id);
    }

    onPublished(packet, client) {
        console.log('CosaServer - Published:', packet.payload);
    }

    onSubscribed(topic, client) {
        console.log('CosaServer - Subscribed:', topic);

        //checks topic is known channel (in this example besiktas)
        if (knownChannels.has(topic)) {
            let coordinates = knownChannels.get(topic);     // gets coordinates of topic (besiktas)
            //make request for forecast from provider(dark sky)
            this.forecastProvider.requestForecastFor(coordinates.latitude, coordinates.longitude).then(
                (forecast) => {
                    // sends forecast data to channel (besiktas)
                    this.publishForecastMessageTo(topic, { status: 200, forecastData: forecast.data });
                },
                (error) => {
                    // sends forecast error to channel (besiktas)
                    this.publishForecastMessageTo(topic, { status: 500, message: 'Hava durumu bilgisi alınamıyor.' });
                });
        }

    }

    onUnsubscribed(topic, client) {
        console.log('CosaServer - Unsubscribed:', topic);
    }

    /**
     * 
     * @param client
     * @param username not used
     * @param password jsonwebtoken encoded client id
     * @param callback
     */
    clientAuthauthentication(client, username, password, callback) {
        let authorized = false;

        if (typeof client !== 'undefined' && client != null && typeof client.id !== 'undefined' && client.id != null) {

            try {
                // tries decodes password token
                let decodedPassToken = jwt.decode(password);
                // checks id
                authorized = decodedPassToken.clientID === client.id;
            }
            catch (e) {
            }

        }

        callback(null, authorized);
    }

    /**
     * publishes message with forecast data for specified channel
     * @param topic channel for publish
     * @param data forecast data
     */
    publishForecastMessageTo(topic, data) {
        let message = {
            topic: topic,
            payload: JSON.stringify(data),
            qos: MQTT_QoS.exactlyOnce,
            retain: false
        };

        this.server.publish(message);
    }


    set settings(value) { this._settings = value; }
    get settings() { return this._settings; }
}

module.exports.CosaServer = CosaServer;
module.exports.CosaServerSettings = CosaServerSettings;