﻿var path = require('path');
var cosaServerModule = require('./server.js');

/**
 * Dark Sky Weather Api settings
 */
var darkSkyForecastSettings = {
    APIKey: '242ecc5faf1c8e1b3a1a9dac0b0bb9af',
    timeout: 100000000
};

/**
 * Mqtt Server Settings
 */
var mqttServerSettings = {
    port: 1883,
    backend: {
        type: 'mongo',
        url: 'mongodb://localhost:27017/mqtt',
        pubsubCollection: 'cosa',
        mongo: {}
    },
    secure: {
        port: 8443,
        keyPath: path.join(__dirname, '/cosa-key.pem'),
        certPath: path.join(__dirname, '/cosa-cert.pem'),
    }
};


var cosaServerSettings = new cosaServerModule.CosaServerSettings(mqttServerSettings, darkSkyForecastSettings);

var cosaServer = new cosaServerModule.CosaServer(cosaServerSettings);
cosaServer.init();