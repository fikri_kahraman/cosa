﻿var assert = require('assert');
var path = require('path');
var cosaServerModule = require('./server.js');
var forecastModule = require('./forecast.js');

exports['serverInitTest'] = function () {

    let darkSkyForecastSettings = {
        APIKey: '242ecc5faf1c8e1b3a1a9dac0b0bb9af',
        timeout: 100000000
    };
    
    let mqttServerSettings = {
        port: 1883,
        backend: {
            type: 'mongo',
            url: 'mongodb://localhost:27017/mqtt',
            pubsubCollection: 'cosa',
            mongo: {}
        },
        secure: {
            port: 8443,
            keyPath: path.join(__dirname, '/cosa-key.pem'),
            certPath: path.join(__dirname, '/cosa-cert.pem'),
        }
    };

    let cosaServerSettings = new cosaServerModule.CosaServerSettings(mqttServerSettings, darkSkyForecastSettings);

    let cosaServer = new cosaServerModule.CosaServer(cosaServerSettings);
    cosaServer.init();

    assert.ok(cosaServer.server != null, 'Server initialize');
    assert.ok(cosaServer.forecastProvider != null, 'Server Forecast Provider initialize');
}

exports['forecastProviderInitTest'] = function () {

    let darkSkyForecastSettings = {
        APIKey: '242ecc5faf1c8e1b3a1a9dac0b0bb9af',
        timeout: 100000000
    };
  
    let forecast = new forecastModule.CosaForecastProvider(darkSkyForecastSettings);

    assert.ok(forecast.forecastProvider != null, 'Forecast Provider initialize');
}

exports['forecastRequestTest'] = function () {

    let darkSkyForecastSettings = {
        APIKey: '242ecc5faf1c8e1b3a1a9dac0b0bb9af',
        timeout: 100000000
    };

    let coordinates = { latitude: 41.044004, longitude: 28.9998113 };

    let forecast = new forecastModule.CosaForecastProvider(darkSkyForecastSettings);
    forecast.requestForecastFor(coordinates.latitude, coordinates.longitude).then((forecast) => {

        assert.ok(typeof forecast !== 'undefined', 'Forecast not undefined test');
        assert.ok(forecast != null, 'Forecast not null test');
        assert.ok(typeof forecast.data !== 'undefined', 'Forecast data not undefined test');
        assert.ok(forecast.data != null, 'Forecast data not null test');
    })
}

//exports.serverInitTest();
//exports.forecastProviderInitTest();
//exports.forecastRequestTest();
