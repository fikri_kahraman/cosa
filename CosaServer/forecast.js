﻿var DarkSky = require('forecast.io');

class CosaForecastProvider {
    constructor(forecastSettings) {
        // Created forecast provider instance
        this.forecastProvider = new DarkSky(forecastSettings);
    }

    /**
     * Creates forecast request for specified latitude, longitude
     * @param latitude
     * @param longitude
     */
    requestForecastFor(latitude, longitude) {
        return new Promise(
            (resolve, reject) => {
                this.forecastProvider.get(latitude, longitude, (err, res, data) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve({ res, data });
                    }
                });
            });
    }
}

module.exports.CosaForecastProvider = CosaForecastProvider;