﻿var mqtt = require('mqtt');
var fs = require('fs');
var jwt = require('jsonwebtoken');


class CosaClient {
    constructor(settings = null) {
        this.settings = settings;
    }

    init() {
        this.clientID = this.generateClientID();
        this.key = fs.readFileSync(this.settings.keyPath);  //reads private key from file
        this.certificate = fs.readFileSync(this.settings.certificatePath);  //reads certificate from file
        this.passwordToken = jwt.sign({ clientID: this.clientID }, this.key, { algorithm: 'RS256' });   //generates private key encoded jsonwebtoken password

        //connection options
        let connectionOptions = {
            port: this.settings.port,
            key: this.key,
            cert: this.certificate,
            clientId: this.clientID,
            password: this.passwordToken,
            rejectUnauthorized: false
        };

        //initialize client connection
        this.client = mqtt.connect(this.settings.host, connectionOptions);
        this.addClientListeners();
    }

    /**
     * Add listeners to client
     */
    addClientListeners() {
        this.client.on('error', this.onError.bind(this));
        this.client.on('connect', this.onConnect.bind(this));
        this.client.on('message', this.onMessage.bind(this));
    }

    onError(e) {
        console.log('CosaClient - Error: ', e.message);
        this.client.end();
    }

    onConnect() {
        console.log('CosaClient - Connect');
        this.client.subscribe(this.settings.channelToSubscribe);
    }


    onMessage(topic, message) {
        console.log('CosaClient - Message');
        try {
            let data = JSON.parse(message.toString());

            switch (data.status) {
                case 200:
                    console.log('Hava durumu bilgisi:', data.forecastData); //logs forecast data
                    break;
                case 500:
                    console.log('HATA: Hava durumu bilgisi alınamıyor.');   //logs forecast cannot receive error
                    break;

                default:
                    throw new Error('Unexpected case occured.');    //unexpected case or data format throws error
            }

        }
        catch (e) {
            console.log('HATA: Beklenmedik bir hata meydana geldi.');   //logs unexpected error message
        }

    }

    /**
     * Generates Client ID
     */
    generateClientID() {
        return 'cosa_' + Math.random().toString(16).substr(2, 8);
    }

    set settings(value) { this._settings = value; }
    get settings() { return this._settings; }
}

module.exports.CosaClient = CosaClient