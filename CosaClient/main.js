﻿var path = require('path');
var cosaClientModule = require('./client.js');

/**
 * Mqtt Client Settings
 */
var settings = {
    port: 8443,
    host: 'mqtts://localhost',
    keyPath: path.join(__dirname, '/cosa-key.pem'),
    certificatePath: path.join(__dirname, '/cosa-cert.pem'),
    channelToSubscribe: 'besiktas'
}

var client = new cosaClientModule.CosaClient(settings);
client.init();