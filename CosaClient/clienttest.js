﻿var assert = require('assert');
var path = require('path');
var cosaClientModule = require('./client.js');



exports['clientInitTest'] = function () {

    let settings = {
        port: 8443,
        host: 'mqtts://localhost',
        keyPath: path.join(__dirname, '/cosa-key.pem'),
        certificatePath: path.join(__dirname, '/cosa-cert.pem'),
        channelToSubscribe: 'besiktas'
    }

    let client = new cosaClientModule.CosaClient(settings);
    client.init();

    assert.ok(client.client != null, 'Client initialize');
}

exports.clientInitTest();